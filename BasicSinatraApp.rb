#!/usr/bin/env ruby
require "socket"
require "ostruct"
require "rubygems"
require "optparse"
require "sinatra"
require 'open3'
require "sinatra/reloader" if development?
include FileUtils::Verbose

 set :bind, '0.0.0.0'
set :port, 4567


#----------------------------

 

get "/image" do
    template = "<h1>Image Tests</h1>
    <p><img src='<%= url('./NCE-Validation.svg') %>' alt='NCE Validation Workflow' />
    <br>
    <img src='./public/NCE-List.jpg' alt='List of NCEs' />
    </p>"
    erb template, :layout => :images
#    erb template, { :locals => params, :images => true }
end


get "/readfile/:folder" do
    # Opening a file
        @@folder2use = "#{params[:folder]}"
        @@fileobject1 = File.open("/fmdata/#{@@folder2use}/CUS_OMNI_DATA.SVOC_ACCOUNT.txt", "r")
#        @@fileobject2 = File.open("/fmdata/#{@@folder2use}/CUS_OMNI_DATA.SVOC_ACCOUNT.txt", "r")      
        template = " <h1>Read a file in the #{@@folder2use} project folder</h1>

        <p><%= Dir.glob('#{@@folder2use}/*.*').join('<br>') %> </p>

        <p>Contents of the <b>#{@@folder2use}</b> folder:</p>

        <p><%= @@fileobject1.read() %> <%= @@fileobject1.close() %></p>"

#        <p><%= File.foreach('#{@@projectname}/CUS_OMNI_DATA.SVOC_ACCOUNT.txt') do |line| ; puts line, $. ; end  %> </p>
#     

#        <p>Contents of the <%= Dir.glob('/fmdata/#{@@projectname}/altered/*.*').join('<br>') %> file: <%= @@fileobject2.read() %> <%= @@fileobject2.close() %></p>

#        <p><%= File.foreach('file.txt') do |line|; puts line, $.;end  %> </p>

        erb template, { :locals => params, :layout => true }

end   

 

get "/readfile" do

    # Opening a file

        @@fileobject1 = File.open('/fmdata/test28/testfile.xml', 'r')

        @@fileobject2 = File.open('/fmdata/test28/altered/house-2-bungalow.xml', 'r')

#         <p>Contents of the <b> /fmdata/test28/testfile.xml </b> file: <br> <b><%= @@fileobject1.read() %> <%= @@fileobject1.close() %></b></p>

#<p>Contents of the <b> /fmdata/test28/altered/house-2-bungalow.xml </b> file: <br> <%= @@fileobject2.read() %> <%= @@fileobject2.close() %></p>

#<br>

        template = " <h1>Read a file in the: #{@@projectname} project folder</h1>

        <p>Contents of the <b>#{@@projectname}</b> folder

        <p><%= Dir.glob('#{@@projectname}/*').each do |filename| ; next if File.directory?(filename) ; @filenamed = (filename) ; puts #@filenamed ; end %> <b> </p>"

    #   <%= @@fileobject = File.open('(filename)', 'r') ; @@fileobject.read() %> <%= @@fileobject.close() %></b> </p>"

#        <br><%= Dir.glob('#{@@projectname}/*.*').join('<br>') %> </p>

 

#        <p><%= Dir.glob('#{@@projectname}/altered/*.*').join('<br>') %> </p>

        erb template, { :locals => params, :layout => true }

end   

 

get "/nce-tests" do

# Perform tests required for NCE validation

@stamp = Time.now

@@ncetest = "/soapuiprojectdata/TestTemplates/NCE-Validation/NCETEST/#{@stamp}"

@@NumOfReturnedFiles = Dir["#{@@ncetest}/ReturnedFiles/*.*"].length ## --ALSO IN BEFORE BLOCK --

# @@fileobject = Dir.entries('#{@@ncetest}') .each do |filename| ; File.open('filename', 'r') do |file| ; end ;end

    template = "<h1>NCE Validation</h1>

    <p>Perform tests to validate Current NCE's</p>

    <img src='<%= url('/NCE-Validation.svg') %>' alt='NCE Validation Workflow' style='width:1080px;height:1080px;'>

    <br>

    <img src='NCE-List.jpg' alt='List of NCEs' style='width:1080px;height:1080px;'>

    <br>

   

    <h1><%= Dir.glob('/soapuiprojectdata/TestTemplates/NCE-Validation/NCETEST/**/*').select {|f| File.directory? f}.join('<br>') %></h1>

    <f>

    <form method='post'>

    Desired Folder Name: <input type='text' name='name' />

    <input type='submit' value='Go!' />

    </form>"

    erb template, { :locals => params, :layout => true }

end

 

post "/nce-tests" do

    @stamp = Time.now

    @@ncetest = "/soapuiprojectdata/TestTemplates/NCE-Validation/NCETEST/#{params[:name]}"

    # When receiving post data from a form field we need to use a "Named Parameter"

    @@projectname = "/soapuiprojectdata/TestTemplates/NCE-Validation/NCETEST/#{params[:name]}"

    source_file = "/soapuiprojectdata/TestTemplates/NCE-Validation/Documentation/NCE_TEST_PROJECT_V1.xml"

    FileUtils.mkdir_p "#{@@ncetest}" unless Dir.exist?"#{@@ncetest}"

    FileUtils.cp(source_file, @@projectname)

    # Count number of files returned   

    template = "

    <P><%= FileUtils.mkdir_p '#{@@ncetest}' unless Dir.exist?'#{@@ncetest}' %></P>

    <h1>Created the project: #{params[:name]} in #{@@projectname} </h1>

    <h1>Changed working directory to '#{params[:name]}' <%= FileUtils.chdir '#{@@projectname}' %>.</h1>

    <br>

    <p> <%= pwd %> </p>"

 

#    <h1>Created the <%= FileUtils.mkdir_p '#{params[:name]}' %> folder.</h1>

#    <h1>Changed working directory to '#{params[:name]}' <%= FileUtils.chdir '#{params[:name]}' %>.</h1>

#    Changed BASH working directory to '#{params[:name]}' <%= `cd/fmdata/'#{params[:name]}'/` %>.

#    Current BASH working directory: <%= `ls/fmdata/'#{params[:name]}'/` %>.

#    Changed BASH working directory to '#{params[:name]}' <%= `cd/fmdata/'#{params[:name]}'/ && ls /fmdata/` %>."

# Dir.mkdir(dir) unless Dir.exist?(dir)

 

#    <h1>Current working directory: <%= FileUtils.pwd %></h1>"

   

    erb template, { :locals => params, :layout => true }

#      erb :upload

end

 

get "/nce-test-run" do

    # Perform tests required for NCE validation

#    FileUtils.chdir '#{@@projectname}'

#    `cp /soapuiprojectdata/TestTemplates/NCE-Validation/Documentation/NCE_TEST_PROJECT_V1.xml '#{@@projectname}' `

    # Execute script in background   

        pid = spawn("`/soapuiprojectdata/RunProject.sh #{@@projectname}`")

        Process.detach(pid)   

        # Count number of files returned   

        template = " <h1>Files within folder: <%= Dir.glob('#{@@projectname}/*.*').join('<br>') %> </h1>

        <p><%= Dir.glob('#{@@projectname}/ReturnedFiles/*.*').join('<br>') %> </p>

        <br>

        <h1>Please wait a few seconds for SoapUI's Testrunner to get going, then click 'View projects files' link </h1>"

#        <h1>Creating the folder: #{@@ncetest}.</h1>

#        <br>

#        Changed BASH working directory to '#{@@ncetest}' <%= `cd /soapuiprojectdata/TestTemplates/NCE-Validation/'#{@@ncetest}'/` %>.

#        Current BASH working directory: <%= #{@@ncetest} %>.

#        Changed BASH working directory to '#{@@ncetest}' <%= `cd /soapuiprojectdata/TestTemplates/NCE-Validation/'#{@@ncetest}'/ && ls /soapuiprojectdata/TestTemplates/NCE-Validation/` %>."

        erb template, { :locals => params, :layout => true }

    end

 

get "/readtestfile" do

    # Opening a file

        @@fileobject1 = File.open('/fmdata/test28/testfile.xml', 'r')

        @@fileobject2 = File.open('/fmdata/test28/altered/house-2-bungalow.xml', 'r')      

        template = " <h1>Read a file in the: #{@@projectname} project folder</h1>

        <p>Contents of the <b> /fmdata/test28/testfile.xml </b> file: <br> <b><%= @@fileobject1.read() %> <%= @@fileobject1.close() %></b></p>

        <p>Contents of the <b> /fmdata/test28/altered/house-2-bungalow.xml </b> file: <br> <%= @@fileobject2.read() %> <%= @@fileobject2.close() %></p>

        <br>

        <p>Contents of the <b>#{@@projectname}</b> folder

        <br><%= Dir.glob('#{@@projectname}/*.*').join('<br>') %> </p>"

#        <p><%= Dir.glob('#{@@projectname}/altered/*.*').join('<br>') %> </p>

        erb template, { :locals => params, :layout => true }

end   

 

get "/readfile2k" do

# Source: https://www.geeksforgeeks.org/file-handling-in-ruby/

    @@fileobject = File.open('/fmdata/test28/testfile.xml', 'r')

    template = "<h1> Read the 1st 2000 lines of an XML file in the #{@@projectname} project folder. </h1>

    <p> <%= @@fileobject.sysread(2000) %> </p>

    <%= @@fileobject.close(); %>"

    erb template, { :locals => params, :layout => true }

end

 

get "/readfilelines" do

# print read lines in array format

    @@fileobject = File.open("/fmdata/#{@@projectname}/testfile.xml", 'r');

    template = "<h1> Read an XML file in the #{@@projectname} project folder</h1>

    <p><%= @@fileobject.readlines %> <%= @@fileobject.close(); %> </p>"                      

 

    erb template, { :locals => params, :layout => true }

end

 

get "/renamesession" do

 

    template = "<h1>Rename the session ID (or any matching pattern) within the uploaded SoapUI project XML file</h1>

    <h1>Current project: <%= @@projectname %></h1>

    <h1>Contents of the <%= @@projectname %> project folder: </h1>   

    <p> <%= Dir.glob('#{@@projectname}/*.xml').join('<br>') %> </p>

    <br>

    <h1>Created the <%= FileUtils.mkdir_p '#{@@projectname}/altered' %> folder to store new file.</h1>

    <f>

        <form method='post'>

        Existing session name: <input type='text' name='oldname' />

        New session name: <input type='text' name='newname' />

        <input type='submit' value='Go!' />

        </form>"

 

    erb template, { :locals => params, :layout => true }

  end

 

post "/renamesession" do

    @@oldtext = params[:oldname]

    @@newtext = params[:newname]

    template = "  <h1>Current project: <%= @@projectname %></h1>

    <p> <%= `cat #{@@projectname}/*.xml | sed 's/#{@@oldtext}/#{@@newtext}/g;' >> #{@@projectname}/altered/#{@@oldtext}-2-#{@@newtext}.xml` %> </p>

    <h1>replaced #{params[:oldname]} with #{params[:newname]}</h1>

    <br>

    <h1> Contents of altered: </h1>

   <p><%= Dir.glob('#{@@projectname}/altered/*.*').join('<br>') %> </p>"

 

    erb template, { :locals => params, :layout => true }

end   

 

get "/stream" do

#  stream do #|template|

#"<h1> <%= `/soapuiprojectdata/RunProject.sh #{@@projectname}` %> </h1>"

    pid = spawn("`/soapuiprojectdata/RunProject.sh #{@@projectname}`")

    Process.detach(pid)

 

        template = "<h1>Executing background run for #{@@projectname} </h1>"

    erb template, { :locals => params, :layout => true }

#  end

end

 

get "/chooseproject" do

   

    template = " <h1>Existing projects </h1>

    <returnbox>

    <b>Available Projects:</b><br><%= Dir.glob('/fmdata/**/*').select {|f| File.directory? f}.join('<br>') %> </p>

    </returnbox>

    <br>

    <br>

    <br>

        <f>

        <form method='post'>

        Enter desired project to load: <br>

        <input type='text' name='name' />

        <br>

        <input type='submit' value='Load Project' />

        </form>"

#    @@projectname = params[:name]

 

    erb template, { :locals => params, :layout => true }

end

 

post "/chooseproject" do

    @@projectname = "/fmdata/#{params[:name]}"

    FileUtils.chdir "#{@@projectname}"

    template = "<h1>Changed working directory to #{@@projectname}.</h1>"

    sleep 1

    redirect to ('/test-tools/files')

 

    erb template, { :locals => params, :layout => true } 

end

 

get "/" do

#    if @@projectname == "" then @@projectname = "Nothing Loaded" else @@projectname = @@projectname end

    template = "
    <h1>Tools in development</h1>
    <p>These modules are currently in development and may not function as expected, stable code will be hosted upon another server TBA. </p>
    <br>
    <p> </p>
    <p> </p>"

#    <h1>Current project directory: <%= @@projectname %></h1>
   erb template, { :locals => params, :layout => true }
end

 

get "/runpro" do

# Execute script in background   

    pid = spawn("`/soapuiprojectdata/RunProject.sh #{@@projectname}`")

    Process.detach(pid)

# Count number of files returned   

    @NumOfReturnedFiles = Dir["#{@@projectname}/ReturnedFiles/*.*"].length

    template = "<h1>Please wait a few seconds for SoapUI's Testrunner to get going, then click 'View projects files' link </h1>"

#    sleep 5

#    redirect to ('/files')

    erb template, { :locals => params, :layout => true }

end

 

get "/new_project" do

    template = "<h1>Create a New Project</h1>

        <p>Create a project to store captured/created data. </p>

        <p>After project creation, you will be asked to upload a SoapUI XML project file to run.</p><br>

        <f>

        <form method='post'>

        Desired Folder Name: <input type='text' name='name' />

        <input type='submit' value='Go!' />

        </form>"

 

   erb template, { :locals => params, :layout => true }

end

 

post "/new_project" do

    # When receiving post data from a form field we need to use a "Named Parameter"

    @@projectname = "/fmdata/#{params[:name]}"

    template = "<h1>Changed working directory to /fmdata/ <%= FileUtils.chdir '/fmdata/' %>.</h1>

    <h1>Creating the folder: #{params[:name]}.</h1>

    <br>

    <h1>Created the <%= FileUtils.mkdir_p '#{params[:name]}' %> folder.</h1>

    <h1>Changed working directory to '#{params[:name]}' <%= FileUtils.chdir '#{params[:name]}' %>.</h1>

    Changed BASH working directory to '#{params[:name]}' <%= `cd/fmdata/'#{params[:name]}'/` %>.

    Current BASH working directory: <%= `ls/fmdata/'#{params[:name]}'/` %>.

    Changed BASH working directory to '#{params[:name]}' <%= `cd/fmdata/'#{params[:name]}'/ && ls /fmdata/` %>."

# Dir.mkdir(dir) unless Dir.exist?(dir)

 

#    <h1>Current working directory: <%= FileUtils.pwd %></h1>"

   

    erb template, { :locals => params, :layout => true }

      erb :upload

end

 

get "/files" do

#    template = "<h1>Changed working directory to '#{params[:name]}' <%= FileUtils.chdir '#{params[:name]}' %>. <br> '#{params[:list]}' </h1>"

 

#  params[:list]

# Count number of files returned   

    @NumOfReturnedFiles = Dir["#{@@projectname}/ReturnedFiles/*.*"].length

  template = "

  <h1>Current project: <%= @@projectname %></h1>

   <h1>Contents of the <%= @@projectname %> project folder: </h1>   

   <p> <%= Dir.glob('#{@@projectname}/*.*').join('<br>') %> </p>

   <h1> Contents of altered: </h1>

   <p><%= Dir.glob('#{@@projectname}/altered/*.*').join('<br>') %> </p>

   <h1> Processed files: <%= #{@NumOfReturnedFiles} %> </h1>

    <h1>Contents of the ReturnedFiles folder: </h1>

    <returnbox>

    <p><%= Dir.glob('#{@@projectname}/ReturnedFiles/*.*').join('<br>') %> </p>

    </returnbox>"

 

#    <p><%= Dir.entries('/fmdata/#{@@projectname}/ReturnedFiles/*.*').map{|e| ('<p>#{e}</p>')} %> </p>

#    <p><%= Dir.glob('/fmdata/#{@@projectname}/ReturnedFiles/*.*').map{|f| f.split('/\n/').last} %> </p>

 

    erb template, { :locals => params, :layout => true }

 

end

 

get "/workdir" do

 

   template = "<h1>Current project directory: <%= @@projectname %></h1>

   <h1> Total number of returned files: <%= Dir['#{@@projectname}/ReturnedFiles/*.txt'].length %> </h1>

   <p> Contained files and folders: </p>

   <p><%= Dir.entries('#{@@projectname}').join('<br>') %></p>



   <returnbox-green>


   </returnbox-green>


   <returnbox-red>

  
   </returnbox-red>

   <br>"

   erb template, { :locals => params, :layout => true }

end

 

get "/upload" do

    template = "<h1>Working directory #{@@projectname} <%= FileUtils.chdir '/fmdata/#{params[:name]}' %>.</h1>

    <br>

    <h1>Current BASH working directory: <%= `pwd` %></h1>

    <h1>Current RUBY working directory: <%= FileUtils.pwd %></h1>"

    erb template, { :locals => params, :layout => true }  

  erb :upload

end

 

post "/upload" do

    tempfile = params[:file][:tempfile]

    filename = params[:file][:filename]

    cp(tempfile.path, "#{@@projectname}/#{filename}")

    template = "<h1>Uploaded: #{filename} to project: #{@@projectname}.</h1>

    <br>

    <br>

    <h1>Click the Run uploaded XML project link then click View projects files to view SOAP progress. </H1>"

    erb template, { :locals => params, :layout => true }

end

 

get "/workdir/:usedir" do

#    @@usethisdir = params[:usedir] # Create global variable

    template = "<h1>Create a project folder named: <%= usedir %>

    <br>

    <%= FileUtils.chdir '#{params[:usedir]}' %> </h1>"

 

   erb template, { :locals => params, :layout => true }

  erb :workdir

end

 

post "/workdir" do

    tempfile = params[:file][:tempfile]

    filename = params[:file][:filename]

    cp(tempfile.path, "/fmdata/#{filename}") #Call global var for folder

    template = "<h1>File upload to <%= '#{params[:usedir]}' %> complete.</h1>"

  erb template, { :locals => params, }   

end

 

get "/update" do

#    template = "<h1> <%= `/soapuiprojectdata/SinatraMenu.sh UpdateSoftware` %> </h1>

    template = "<h1> <%= `cd /soapuiprojectdata/ && git pull` %> </h1>

    <h1>Last updated: <%= Time.now %> </h1>"

   erb template, { :locals => params, :layout => true }

end

 

post "/update" do

# Post request to handle update webhook

        template = "<h1> <%= `cd /soapuiprojectdata/ && git pull` %> </h1>

        <h1>Last updated: <%= Time.now %> </h1>"

       erb template, { :locals => params, :layout => true }

    end

 

get "/runpro/:thisproject" do

    @usethisproject = params[:thisproject]

    template = "<h1> <%= `/soapuiprojectdata/RunProject.sh` %></h1>"

    erb template, { :locals => params, :layout => true }

end

 

get "/view/:folder" do

  'Hello #{params[:folder]}'

  template = "<h1> <%= :list %>"

  params[:list]

  list = Dir.glob("/fmdata/*.*").join('<br>')  

#  erb template, { :locals => params, }

  # render list here

end

 

get "/time" do

  template = "<h1> current date & time is: <%= Time.now %></h1>"


   erb template, { :locals => params, :layout => true }

end


get "/who" do
    %q{
        Hello World!<br><f>
        <form method="post">
        Enter your name: <input type="text" name="name" />
        <input type="submit" value="Go!" />
        </form>
    }
end

 

post "/who" do

    # When receiving post data from a form field we need to use a "Named Parameter"

    "Hello #{params[:name]}"

end


get "/pagemaker" do

    %q{

        Please input desired field data.<br><f>

        <form method="post">

    <ul>

        <li>Enter your desired title: <input type="text" name="title" /></li>

        <li>Enter your desired facebook_URL: <input type="text" name="facebook_URL" /></li>

        <li>Enter your desired instagram_URL: <input type="text" name="instagram_URL" /></li>

        <li>Enter your desired twitter_URL: <input type="text" name="twitter_URL" /></li>

        <li>Enter your desired logo_source: <input type="text" name="logo_source" /></li>

        <li>Enter your desired moto: <input type="text" name="moto" /></li>

        <li>Enter your desired chapter_heading: <input type="text" name="chapter_heading" /></li>

        <li>Enter your desired chapter_paragraph: <input type="text" name="chapter_paragraph" /></li>

        <li>Enter your desired chapter_image: <input type="text" name="chapter_image" /></li>

        <li>Enter your desired chapter_heading0: <input type="text" name="chapter_heading0" /></li>

        <li>Enter your desired chapter_paragraph0: <input type="text" name="chapter_paragraph0" /></li>

        <li>Enter your desired chapter_image0: <input type="text" name="chapter_image0" /></li>

        <li>Enter your desired chapter_heading1: <input type="text" name="chapter_heading1" /></li>

        <li>Enter your desired chapter_paragraph1: <input type="text" name="chapter_paragraph1" /></li>

        <li>Enter your desired chapter_image1: <input type="text" name="chapter_image1" /></li>


        <li>Enter your desired email: <input type="text" name="email" /></li>
        <li>Enter your desired phone: <input type="text" name="phone" /></li>
        <li>Enter your desired map: <input type="text" name="map" /></li>

       
    </ul>
        <input type="submit" value="Go!" />
        </form>

    }

 

end

 

post "/pagemaker" do

    # When receiving post data from a form field we need to use a "Named Parameter"

    " <h1>Collected Data</h1>

    <p><h1>You entered <b>#{params[:title]}</b> for title</h1>

    <h1>>You entered <b>#{params[:facebook_URL]}</b> for facebook_URL</h1>

    <h1>You entered <b>#{params[:instagram_URL]}</b> for instagram_URL</h1>

    <h1>You entered <b>#{params[:twitter_URL]}</b> for twitter_URL</h1>

    <h1>You entered <b>#{params[:logo_source]}</b> for logo_source</h1>

    <h1>You entered <b>#{params[:moto]}</b> for moto</h1>

    <h1>You entered <b>#{params[:chapter_heading]}</b> for chapter_heading</h1>

    <h1>you entered <b>#{params[:chapter_paragraph]}</b> for chapter_paragraph</h1>

    <h1>You entered <b>#{params[:chapter_image]}</b> for chapter_image</h1>

    <h1>You entered <b>#{params[:chapter_heading0]}</b> for chapter_heading0</h1>

    <h1>you entered <b>#{params[:chapter_paragraph0]}</b> for chapter_paragraph0</h1>

    <h1>You entered <b>#{params[:chapter_image0]}</b> for chapter_image0</h1>

    <h1>You entered <b>#{params[:chapter_heading1]}</b> for chapter_heading1</h1>

    <h1>you entered <b>#{params[:chapter_paragraph1]}</b> for chapter_paragraph1</h1>

    <h1>You entered <b>#{params[:chapter_image1]}</b> for chapter_image1</h1>

 

    <h1>You entered <b>#{params[:email]}</b> for email</h1>

    <h1>you entered <b>#{params[:phone]}</b> for phone</h1>

    <h1>You entered <b>#{params[:map]}</b> for map </h1></p>

    <br>

    <h1> JSON format </h1>   

    <p>title:#{params[:title]}

    <br>facebook_URL:#{params[:facebook_URL]}

    <br>  instagram_URL:#{params[:instagram_URL]} 

    <br>  twitter_URL:#{params[:twitter_URL]} 

    <br>  logo_source:#{params[:logo_source]} 

    <br>  moto:#{params[:moto]} 

    <br>  chapter_heading:#{params[:chapter_heading]} 

    <br>  chapter_paragraph:#{params[:chapter_paragraph]} 

    <br>  chapter_image:#{params[:chapter_image]} 

    <br>  chapter_heading0:#{params[:chapter_heading0]} 

    <br>  chapter_paragraph0:#{params[:chapter_paragraph0]} 

    <br>  chapter_image0:#{params[:chapter_image0]} 

    <br>  chapter_heading1:#{params[:chapter_heading1]} 

    <br>  chapter_paragraph1:#{params[:chapter_paragraph1]} 

    <br>  chapter_image1:#{params[:chapter_image1]} 

 

    <br>email1:#{params[:email]}
    <br>phone:#{params[:phone]}
    <br>map:#{params[:map]} </p>"
#    template = " <p> Return <%= @returneddata %></p> "
#   erb template, { :locals => params, :layout => true }
end

 

 

 

# This example uses 'Named Parameters' (e.g. params[:name])

get "/add/:a/:b" do

    # http://127.0.0.1:9393/add/5/1 which should display 6 (5+1)

    (params[:a].to_i + params[:b].to_i).to_s

    

end

 

# This example uses 'block parameters' (e.g. |x, y, z|)

get "/subtract/:a/:b" do |a, b|

    # Go to http://127.0.0.1:9393/subtract/5/1 which should display 4 (5-1)

    (a.to_i - b.to_i).to_s

end

 

=begin

    The following example shows how to use Templates and Layouts.

    A Template is HTML code interpolated with dynamic content.

    A Layout is a wrapper around the Template (e.g. <html><body>TEMPLATE</body></html> which means you don't have to include the same page content around your Template)

    Templating is handled via Erb, but there are other template languages you can use

    instead such as HAML or Builder

=end

 

# The "before" code block is executed before ALL requests

before do

#    @@projectname = "no project selected..."

 

@people = [

        { :name => "Mark", :age => 30 },

        { :name => "Brad", :age => 21 },

        { :name => "Ash", :age => 21 }

    ]

end

 

# you must have a folder called 'views' with your template & layout files within it

# e.g. /views/mylayout.erb and /views/mytemplate.erb (see additional files below for the content of these files)

get "/erb-template-external" do

   

    erb :mytemplate, :layout => :mylayout

end

 

# Sources: https://krisingva.github.io/jekyll/update/2021/02/12/forms-in-sinatra-app.html

 

 

__END__

 

@@NumOfReturnedFiles = Dir["#{@@projectname}/ReturnedFiles/*.*"].length

 

@@upload

<form action='/test-tools/upload' enctype="multipart/form-data" method='POST'>

    <input name="file" type="file" />

    <input type="submit" value="Upload" />

</form>

 

@@workdir

<form action='/test-tools/workdir' enctype="multipart/form-data" method='POST'>

    <input name="file" type="file" />

    <input type="submit" value="Upload" />

</form>

 

 
