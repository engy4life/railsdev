#!/bin/bash 

# Dev tools for rails configuration menu.
# Designed for container usage (Docker) hosted on OL7.

CreateViewsFromControllers(){
# Make controller and add a data output variable to views.
  rm-rf $OutputFiles/Controller.txt
  rm -rf $OutputFiles/Views.txt
  OutputFiles="$HOME/railsdev/projects/$ProjectName"
  RunSoap="/soapuiprojectdata/SoapcmdUI.sh"
  ModulesList=( AddProxy ) # CloneInputRepo PullInput CloneOutputRepo PushOutputRepo ToolUpdate ViewFolder )
  for i in "${ModulesList[@]}"
  do
  echo  "  @$i = BT $RunSoap $i BT " | sed 's#BT#`#' | >> $OutputFiles/Controller.txt
  echo  "  <p> <%= @$i %> </p> " >> $OutputFiles/Views.txt
  done
  ls $OutputFiles/*.txt
  more $OutputFiles/Controller.txt
  more  $OutputFiles/Views.txt
}


FileUpload(){
# create rails project to upload files required for SoapUI API run application for each tv01, ufast01, 02, 03 BPREP & PROD etc. in parrallel.
# Source: https://www.microverse.org/blog/how-to-build-an-image-upload-feature-with-rails-and-active-storage
    ProjectName="testfile"
# create new project
    rails new $HOME/railsdev/projects/$ProjectName
    cd $HOME/railsdev/projects/$ProjectName
# Setup active storage

    rails active_storage:install
# migrate to update db
    rails db:migrate
# Edit storage configuration.
    nano $HOME/railsdev/projects/$ProjectName/config/storage.yml
#
}

# https://www.timdisab.com/uploading-files-in-ruby-on-rails-with-active-storage
 

# ---------------------- Arrays & Variables ---------------------
  RED='\033[0;41;30m'
  STD='\033[0;0;39m'
  T=$(date +"%T")
  D=$(date +"%F")
  # Set git push default to stop that message appering
  git config --global push.default simple
  # Create projects folder
  cd ~/railsdev/projects

  DataTypes=("binary" "boolean" "date" "datetime" "decimal" "float" "integer" "string" "text" "time" "timestamp")

# --------------------- Install and setup ---------------------


InstallTools(){
# Tasks which must be perfomed as root within desired container. 
  sudo apt update
  sudo apt upgrade -y
  sudo apt autoremove -y
# instal required dev tools
  sudo apt install -y sudo yarn curl bash-completion nano make g++ ruby-dev unixodbc unixodbc-dev
# update ruby gems
#  gem update --system
# Install required gems for file upload
  gem install rails
# Gems for oracle-db queries
  gem install dbi dbd-odbc ruby-odbc 
# Export folder paths to .bashrc for oracle client
  echo "PATH='$HOME/railsdev/instantclient_21_1/:$PATH'" >> $HOME/.bashrc && export PATH
  echo "PATH='$HOME/railsdev/sqlcl/:$PATH'" >> $HOME/.bashrc && export PATH
 echo "ORACLE_HOME='$HOME/railsdev/instantclient_21_1/:$PATH'" >> $HOME/.bashrc
  echo "LD_LIBRARY_PATH='$HOME/railsdev/instantclient_21_1/:$PATH'" >> $HOME/.bashrc
  gem install ruby-oci8 activerecord-odbc-adapter # activerecord-oracle-adapter --source http://gems.rubyonrails.org
}

Soft2Install(){
# Install some software of choice
# Capture desired software
  read -rp "add list of desired software" Software2Get
  sudo apt update
  sudo apt install $Software2Get -y
  sudo apt autoremove -y
}


# ---------------------- Project Creation ---------------------

# Create a new rails project & git init
 

NameProject(){
# Create a new rails project
    read -rp "Name of this project: " ProjectName
# Ensure a name was entered or stop.
    if [ $ProjectName = "" ]
    then
    echo "A name is rquired"
    else echo $ProjectName
    fi
}

ProjectSelect(){
# Display a list of projects  
  ls ~/railsdev/projects/
  read -rp "Choose a project " ProjectName
  if [ $ProjectName == "" ]
  then
  ProjectName="alpha"
  echo "No project selected, using Dev Folder:" $ProjectName
  else
  echo "Using selected folder: " $ProjectName
  fi
  cd ~/railsdev/projects/$ProjectName
  }

NewRorApp(){
    NameProject
    mkdir -vp ~/railsdev/projects/$ProjectName
    cd ~/railsdev/projects/$ProjectName
  #  rvm use ruby-2.6.0@$ProjectName --ruby-version --create
  #  gem install rails
    rails new .
  #  bundle binstubs bundler
  #  bundle install
}

GitInit(){
# Initialize Git within the created folder
    git init ~/railsdev/projects/$ProjectName
}

ValidateProjectCreation(){
#  confirm the existance of ~/projects/$ProjectName and then initialize git.
if [ -f "~/railsdev/projects/$ProjectName" ];
then
cd ~/railsdev/projects/$ProjectName
lastmessage="$ProjectName is ready.";
else
lastmessage="That did not work, check above for clues."
fi
}

# ---------- Options to manage the server --------------

RailsUp(){
  #Start the server and configure for localhost:3000. 
    rails s -b 0.0.0.0 #-d
}

RailsUpBackground(){
  #Start the server in the background and configure for localhost:3000. 
    rails s -b 0.0.0.0 &
    ProcessID=$(cat ~/railsdev/projects/$ProjectName/tmp/pids/server.pid)
    echo "PID for" $ProjectName "is " $ProcessID
    ps aux
}

StopRailsBackground(){
# view running process which include the word spring
    ps aux
    KillAppPID=$( ps aux | grep "bin/rails s -b 0.0.0.0 -d" | awk '{ print $2 }' )
    PID2Kill=$( ps aux | grep "spring server | $ProjectName" | awk '{ print $2 }' )
    PumaPID2Kill=$( ps aux | grep "puma " | awk '{ print $2 }' )
    echo "spring server | $ProjectName is PID:" $PID2Kill
    echo "puma PID:" $PumaPID2Kill
    kill $PumaPID2Kill
    kill $PID2Kill
    kill $KillAppPID
    ps aux
} 

ListPIDs(){
   ps aux | grep "bin/rails s -b 0.0.0.0 -d"
   ps aux | grep "spring server | $ProjectName"
   ps aux | grep "puma "
}

# ------------- Creation & Editing --------------------

CreatePage(){
# Creates a new page with a controller to host app module with input/output.
# Capture Page name:
  read -rp "Please enter an name for this page: " Newpage
  if [ $Newpage == ""]
  then echo "Cannot proceed without a name...."
  else rails g controller $ProjectName $Newpage
  fi
  lastmessage="Created $Newpage in $ProjectName"
}

ShowControllers(){
# Display controllers to edit
  rake routes
  ls ~/railsdev/projects/$ProjectName/app/controllers/*.rb
}

EditController(){
  read -rp "Choose controller to edit: " EditThisController
  if [ $EditThisController == "" ]
  then echo "Need a controller name... "
  else nano ~/railsdev/projects/$ProjectName/app/controllers/$EditThisController
  fi
}

SetupHomePage(){
# Creates a page which will contain links to the various resource index pages.
    rails g controller $ProjectName index
# Set created page as root (index)
    sed -i "s/.*/root '$ProjectName#index'/" ~/railsdev/projects/$ProjectName/config/routes.rb
# Display created files
    more ~/railsdev/projects/$ProjectName/config/routes.rb
}


EditModel(){
# show views in project folder
  ls ~/railsdev/projects/$ProjectName/app/models/*.rb
# Open created file for editing.
  read -rp "Choose model to edit: " EditThisView
  if [ $EditThisView == "" ]
  then echo "Need a file name... "
  else nano ~/railsdev/projects/$ProjectName/app/models/$EditThisView
  fi
}


EditPostsViewPages(){
# show views in project folder
  ls ~/railsdev/projects/$ProjectName/app/views/posts/
# Open created file for editing.
  read -rp "Choose view to edit: " EditThisView
  if [ $EditThisView == "" ]
  then echo "Need a file name... "
  else nano ~/railsdev/projects/$ProjectName/app/views/posts/$EditThisView
  fi
}

 
EditRoutes(){
# manually add a GET or Resource to a controller module.
  nano  ~/railsdev/projects/$ProjectName/config/routes.rb
}


EditFile(){
# Edit a selected file
# Display a list of files in project folder
  ls $HOME/railsdev/projects/$ProjectName/
# Capture file to edit.
  read -rp "Select a file to edit: " File2Edit
# Open file in nano.
  nano $HOME/railsdev/projects/$ProjectName/$File2Edit
}

EditHomePage(){
# Open created file for editing.
    nano ~/railsdev/projects/$ProjectName/app/views/rewq/index.html.erb
}

FormMaker(){
# Create Scenario Task Action Result capture form
  rails g scaffold WorkRequest Work_request_id:integer 
# Update datbase
  rake db:migrate
} 

NewTable(){
# Create a data capture page mapped to the internal database for manipulation.
  cd ~/railsdev/projects/$ProjectName/
#create scaffolding for Database object CRUD:
  read -rp "Name for this new page/DB table: " Table_Name
  printf '%s\n' "${DataTypes[@]}"
  read -rp "Name for the new input field:Datatype - " Field_Name
# Display available data data types

# Display data to generate
  echo "" $Table_Name $Field_Name
# Execute captured data 
  rails g scaffold $Table_Name $Field_Name
# update db 
  rails db:migrate
  #MoreTables
} 


NewController(){
# Create a controller for a
    read -rp "Name for this new DB table: " Table_Name
    rails generate controller $Table_Name new
}


NewModel(){
# Create a new field for existing db table/page
    ProjectSelect
    read -rp "Name for this new field: " Field_Name
    printf '%s\n' "${DataTypes[@]}"
    read -rp "Data Type: " Data_Type
    rails generate model $Table_Name $Field_Name:$Data_Type
    rake db:migrate
}

MoreTables(){
# Add more tables
  read -rp "Add another field to this table? Y/N" More_Fields
} 

NewColumn(){
# Add a new column:datatype to an existing field:
    read -rp "Add a field to which table? " ThisTable
    read -rp "name this new field: " ThisItem
    read -rp "Data Type: " Data_Type
    rails generate migration Add$ThisItem To $ThisTable
    $ThisItem:$Data_Type
    rails db:migrate
}


# ------------------- Removal ---------------------
#Delete a scaffold and database entry:fibre_delivery_activity
DelTable(){
  ProjectSelect
  read -rp "Name of DB table to Delete/Drop: " Table_Name
  rails destroy $Table_Name
  rails db:migrate
  rails db:drop:$Table_Name
  rails db:migrate
}

DelScaffold(){
  read -rp "Name of the Scaffold to remove: " Scaff_Name
  rails destroy $Scaff_Name
  rails db:migrate
}

DeleteProject(){
    ProjectSelect
    rm -rf ~/railsdev/projects/$ProjectName
}

# 3 -------------------- GIT Commands -------------------------

gitUserConfig(){
read -rp "Enter your email: " gitmail
git config user.email "$gitmail"
read -rp "Enter your username: " gituser
git config user.name "$gituser"
}

TryATing(){
    NewRorApp
    GitInit
    #ProjectSelect
    SetupHomePage
    RailsUp
}

UpdateSoftware(){
  cd ~/railsdev
  cd /railsdev
  git add .
  git add **/*
  git add -A
  git stash
  git pull
  echo " Exited to apply update.. " && exit 0
}


# ----------------- Display menu options -----------------

MainMenu(){
# Create ROR apps.
    MenuTitle="Ruby on Rails Dev Tool"
    Description="Manage & develop RoR Projects from a BASH menu driven interface"
show_menus() {
echo " "
echo " $MenuTitle "
echo " $Description "
echo -e "Current Project: ${RED} $ProjectName ${STD}"
echo "-----------------------------------"
echo "0.  Update this App."
echo "5.  Install desired software"
echo "10. Install RoR tools. "
echo "11. New                  Create a new RoR project " #& GIT init "
echo "12. Select Project"
echo "13. Start Server         Start the Rails server.  "
echo "14. Start server in the background. "
echo "15. Create a home/index page. "
echo "16. Edit the new home page. "
echo "17. Stop a background server. "
echo "18. Select a project file to edit."
echo "19. Edit routes file."
echo "20. Create resource, page and Database table for interaction (CRUD). "
echo "21. Add a data field to an existing page (new column)."
echo "22. New Model"
echo "23. Create a new page within the current project."
echo "24. View existing Controllers"
echo "25. Edit Controller"
echo "26. select and edit a model."
echo "27. Edit POST View files."
echo "28. Create SoapUI interface components"
echo "31. File upload. "

echo "6.  Things in the background.. "

echo "70. Delete a page "
echo "72. Delete Scaffold "
echo "75. Delete Project"

echo "8.  Create a capture form from a file"

echo "90. Trying a Thing"
echo "95. "
echo "99. Quit "
echo " "
echo " $lastmessage "
echo "Existing Projects: " && ls ~/railsdev/projects
ListPIDs
}


read_options(){
local choice
read -p "Enter the desired item number: " choice

case $choice in
  0)   UpdateSoftware ;;
  5)  Soft2Install ;;
  10)  InstallTools ;;

  11)  NewRorApp ;;
  12)  ProjectSelect ;;

  13)  RailsUp ;;
  14)  RailsUpBackground ;;

  15)  SetupHomePage ;;
  16)  EditHomePage ;;

  17)  StopRailsBackground ;;
  
  18)  EditFile ;;
  19)  EditRoutes ;;
  20)  NewTable ;;
  21)  NewColumn ;;
  22)  NewModel ;;
  23)  CreatePage ;;
  24)  ShowControllers ;;
  25)  ShowControllers && EditController ;;
  26)  EditModel ;;
  27)  EditPostsViewPages ;;
  28)  CreateViewsFromControllers ;;
  
  31)  FileUpload ;;
  6)  ps aux | grep 'spring' ;;
  70) DelTable ;;
  72) DelScaffold ;;
  75) DeleteProject ;;
  8)  FormMaker ;;
  90) TryATing ;;
  95)  ;;
  99) clear && echo " Goodbye $USER! " && exit 0;;
  *) echo -e "${RED} sending $choice to bin/bash.....${STD}" && echo -e $choice | /bin/bash
esac
}

# --------------- Main loop --------------------------

while true
do
show_menus
read_options
done
}

    if [ $1 == ""]
    then RunModule="MainMenu"
    else RunModule=$(echo "$1 $2")
    fi
    $RunModule

# This is the selector, used to run through modules

#MainMenu
#ProjectList
#NameProject
#NewRorApp
#ValidateProjectCreation
#ProjectSelect
#GitInit
#TryATing
#SetupHomePage
#RailsUp
