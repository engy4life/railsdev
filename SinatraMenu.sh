#!/bin/bash 

#--- Variables ---
SAI="sudo apt install -y"
SGI="sudo gem install"

SetupSinatra(){
# Prepare all requirements for Sinatra
    $SAI ruby ruby-dev nano
    $SGI sinatra sinatra-contrib thin xfiles
}

UpdateSoftware(){
  cd /railsdev
  git add .
  git stash
  git pull
  echo " Exited to apply update.. " && exit 0
}

OpenInBrowser(){
        firefox localhost:4567
}

RunBasicSinatraApp(){
    ruby BasicSinatraApp.rb 
}

RunDevApp(){
  ruby dev.rb
}

RunDevAppAndDisown(){
  ruby dev.rb &
}

EditApp(){
  nano dev.rb
}

MainMenu(){
# Create Sinatra apps.
    MenuTitle="Sinatra Dev Tool"
    Description="Manage & develop Sinatra Projects from a BASH menu driven interface"

show_menus(){
echo " "
echo " $MenuTitle "
echo " $Description "
echo -e "Current Project: ${RED} $ProjectName ${STD}"
echo "-----------------------------------"
echo "0.  Update this App."
echo "10. Install Sinatra requirements. "
echo "20. Run Sinatra App. "
echo "50. Run Dev App "
echo "51. Run Dev App in the background"
echo "30. Open in Firefox "
echo "40. Edit Dev app."
echo "95. "
echo "99. Quit "
echo " "
echo " $lastmessage "
 
}

read_options(){
local choice
read -p "Enter the desired item number: " choice

case $choice in
  0)   UpdateSoftware ;;
  10)  SetupSinatra ;;
  20) RunBasicSinatraApp ;;
  50) RunDevApp ;;
  51) RunDevAppAndDisown ;;
  30) OpenInBrowser ;;
  40) EditApp ;;
  95)  ;;
  99) clear && echo " Goodbye $USER! " && exit 0 ;;
  *) echo -e "${RED} sending $choice to bin/bash.....${STD}" && echo -e $choice | /bin/bash
esac
}

# --------------- Main loop --------------------------

while true
do
show_menus
read_options
done
}

#--- Switch Handler ---

    if [ $1 == ""]
    then RunModule="MainMenu"
    else RunModule=$(echo "$1 $2")
    fi
    $RunModule

